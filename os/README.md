# Ansible OS role

This role is base role for other roles.

## Description

This role act like base role with optional features (see Variables). The supported features are: apt, console, grub, hw, kernel, locale, log, net, ssh, popcon, ufw. Features are in fact conditional dependencies to `os_*` roles.

## Variables

Features can be configured by setting feature dict value to boolean: true = use, false = don't use. This role uses `_os` dict as it's namespace. The features can be set by setting host / group variables in `os` dict.

## Version History

- RC3: Documented
- RC2: Refactored
- RC1: Introduced - initial version
