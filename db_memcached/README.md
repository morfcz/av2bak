# AV2 Memcached

Memcached Role.

## Variables

db_memcached:
  enabled: true|false
  group: string
  options: {}
  options_default: don't touch
  packages: []
  pam_limits: []
  service: string
  socket_dir: string(abs path)
  user: string

## Versions

- RC3: refactored
- RC2: introduced

## Usage

Basically like any other role, configure using db_memcached.options dict.
